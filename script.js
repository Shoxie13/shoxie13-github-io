/* Add any JavaScript you need to this file. */

window.onload = function() {
    defaults();
    document.getElementById('question').onclick = check;
    document.getElementById('comment').onclick = check;
    document.getElementById('order').onclick = check;
}

function defaults() {
    document.getElementById('order-number').style.display = 'none';
}

function check() {
    if (document.getElementById('order').checked) {
        document.getElementById('order-number').style.display = 'block';
    }else {
        document.getElementById('order-number').style.display = 'none';
    }
}

function shoes() {
    document.querySelectorAll('.portfolio-item-wrapper').style.display = 'none';
}